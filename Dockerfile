#+++++++++++++++++++++++++++++++++++++++
# Dockerfile for teufels/php-fpm
#+++++++++++++++++++++++++++++++++++++++

FROM php:8.0-fpm-buster

COPY etc/             /opt/docker/etc/

# apt
RUN apt-get update \
&& apt-get install -y xz-utils \
&& apt-get install -y libxrender1 \
    && apt-get install -y \
    apt-transport-https \
    lsb-release \
    libbz2-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    libpq-dev \
    libxml2-dev \
    libxslt1.1 libxslt1-dev \
    libc-client-dev libkrb5-dev \
    ca-certificates \
    wget \
    nano \
    mariadb-client \
    zsh \
    git \
    gdebi \
    build-essential libssl-dev libxrender-dev \
    gnupg2 \
    sudo \
    vim \
    zip \
    unzip \
    imagemagick \
    graphicsmagick \
    ghostscript \
    gsfonts \
    jpegoptim \
    optipng \
    webp \
    jpegoptim \
    libicu-dev \
    libzip-dev

# pecl
# https://stackoverflow.com/questions/47671108/docker-php-ext-install-mcrypt-missing-folder
RUN pecl install apcu \
	&& pecl install mcrypt-1.0.4 \
	&& docker-php-ext-enable apcu mcrypt

# docker-php-ext-install
RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
	&& docker-php-ext-install imap \
	&& docker-php-ext-configure intl \
	&& docker-php-ext-install intl \
	&& docker-php-ext-install opcache \
	&& docker-php-ext-install bcmath \
	&& docker-php-ext-install calendar \
	&& docker-php-ext-install bz2 \
	&& docker-php-ext-install ctype \
	&& docker-php-ext-install dba \
	&& docker-php-ext-install exif \
	&& docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ \
	&& docker-php-ext-install gd \
	&& docker-php-ext-install gettext \
	&& docker-php-ext-install mysqli \
	&& docker-php-ext-install pdo_mysql \
	&& docker-php-ext-install pgsql \
	&& docker-php-ext-install soap \
	&& docker-php-ext-install xsl \
	&& docker-php-ext-install zip \
	&& docker-php-ext-enable opcache bcmath calendar bz2 ctype dba gd gettext exif imap mysqli pdo_mysql pgsql soap xsl zip

# # wkhtml
#RUN wget https://bitbucket.org/wkhtmltopdf/wkhtmltopdf/downloads/wkhtmltox-0.13.0-alpha-7b36694_linux-jessie-amd64.deb \
RUN gdebi --n /opt/docker/etc/wkhtmltox_0.12.5-1.stretch_amd64.deb

RUN docker-php-ext-install \
    sockets



# oh-my-zsh
RUN wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh || true \
	&& chsh -s /bin/zsh \
	&& chsh -s /bin/zsh www-data

RUN ln -sf /opt/docker/etc/php/development.ini /usr/local/etc/php/conf.d/php.ini

WORKDIR /app

# imagick
RUN apt-get update && apt-get install -y libmagickwand-dev ghostscript \
    && pecl install imagick \
    && docker-php-ext-enable imagick

# link xdebug config for osx
#RUN ln -sf /opt/docker/etc/php/xdebugfordockerformac.ini /usr/local/etc/php/conf.d/xdebugfordockerformac.ini

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - \
 && apt-get update \
 && apt-get install -y \
 nodejs